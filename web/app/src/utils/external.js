function format(sec, form){
    if (sec == null || sec == "undefined"){return null}
    if (form === "hms" || form === "hm"){
            let date_str = new Date(sec*1000).toISOString()
            if (form === "hm"){return date_str.substr(11, 5)}
            else {return date_str.substr(11, 8) + " (" + Math.round(sec*1/36)+ " cent)"}
    }
    else { return sec}
}

export function create_logbook(json) {
	if (json == null || json == "undefined") {json = {airfield: {}, flights: []}}
	let devices = json.devices;
	let flights = json.flights;
	let flight_warning = false
	let warning = false
	let warningdict = {}
	let update_expected = false
	for (var idx=0; idx < flights.length; idx++){
		let flight = flights[idx];
		flight.device = devices[flight.device];
		flight.glider = (flight.device.aircraft_type === 1)
		flight.unknown = (flight.device.aircraft_type === 0)
        flight.duration_hm = format(flight.duration, "hm");
        flight.duration_hms = format(flight.duration, "hms");
		flight.duration_expected_hm = format(flight.duration_expected, "hm")
		update_expected ||= flight.duration_expected
		flight.tow = flights[flight.tow];
		// identifie the flight as a possible towing flight (for display in case of "ghost" glider, issue #20)
		if (typeof flight.tow === "undefined" && !flight.glider && !flight.unknown && (flight.duration !== null) && (flight.duration < 1200) && !flight.towing){
			flight.tow = {}
			flight.tow.duration_hms = flight.duration_hms;
			flight.tow.max_alt = flight.max_alt;
			flight.tow.max_height = flight.max_height;
			flight.tow.stop_delta = flight.stop_delta;
			flight.tow.stop_big_delta = flight.stop_big_delta;
		}
		flight_warning = false;
		(flight.glider && (flight.start!==null)) ? flight_warning = false : '';
		(flight.glider && (flight.stop===null)) ? flight_warning = true : '';
		flight.warning = flight_warning && flight.warn
		warningdict[flight.device.address] = flight.warning;
		flight.start_delta = flight.start_delta || 0;
		(flight.start_delta < 60) ? flight.start_big_delta = false : flight.start_big_delta = true;
		flight.stop_delta = flight.stop_delta || 0;
		(flight.stop_delta < 60) ? flight.stop_big_delta = false : flight.stop_big_delta = true;
	}
	for (let k in warningdict){
		if (warningdict[k]) {
			warning = true
			break
		}
	}
	json.warning = warning
	json.update_expected = update_expected // flag to indicate if a compute expected duration is needed
	return json
}

export function update_expected(flights){
	for (var idx=0; idx < flights.length; idx++){
		let flight = flights[idx];
		if (flight.duration_expected){
			flight.duration_expected += 10;
			flight.duration_expected_hm = format(flight.duration_expected, "hm");
		} else flight.duration_expected_hm = null;
	}
	return flights
}


export function nullable(obj, key) {return key.split(".").reduce(function(o, x) {return (typeof o == "undefined" || o === null) ? null : o[x];}, obj)}

// altitude converter m -> ft
export function altconv(alt, bol){
	if (typeof alt === "undefined" || alt === null) {return null}
	else { return (bol ? `${Math.round(alt*3.2808399)} ft` : `${alt} m`)}
	}

// ground speed converter km/h -> knts
export function gspconv(gsp, bol){
	if (typeof gsp === "undefined" || gsp === null) {return null}
	else { return (bol ? `${Math.round(gsp*0.54)} kts` : `${gsp} km/h`)}
	}

// climb rate converter m/s -> 100 * ft/min
export function clrconv(clr, bol){
	if (typeof clr === "undefined" || clr === null) {return null}
	else { return (bol ? `${Math.round(clr*19.685)/10} ft/min` : `${clr} m/s`)}
	}