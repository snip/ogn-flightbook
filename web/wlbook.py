from json import loads
from collections import OrderedDict

from pytz import timezone as pytz_timezone, utc as pytz_utc

from core.eph import Eph
from core.logbook import Logbook
from core.queue_ import Status
from utils.device import mapping_addresses, Device


class WLogbook(Logbook):
    """
    a SubClass of LogBook with method for json export and airfield class member
    json export take care of privacy
    """
    def __init__(self, date, code, red, db):
        airfield = {"code": code, "name": None, "elevation": None, "time_info": None, "latlng": None}
        match_airfield = db.match_airfield(code)
        tz = pytz_utc
        lnglats = None
        if match_airfield is not None:
            airfield["name"] = match_airfield[0]
            airfield["elevation"] = match_airfield[1]
            tz = pytz_timezone(match_airfield[2])
            airfield["country"] = match_airfield[3]
            lnglats = red.geopos("GEO:L", code)
            if len(lnglats) == 1:
                airfield["latlng"] = [round(x, 5) for x in lnglats[0][::-1]]

        Logbook.__init__(self, date, code, red, db, tz)
        if airfield["latlng"] is not None:
            eph = Eph(lnglats=lnglats, elev=airfield["elevation"], date=self.date, tz=tz)
            airfield["time_info"] = eph.info

        self.airfield = airfield

    def to_json(self):
        """
        Json string repr for flightbook. Circular refs to flight or nested devices objects
        are identified as index of respective flights or devices json array.
        ie: {devices:[{first_json_device, second_json_device], ...]
             flights: [{device: 1, ...}, {device: 0, ..}, ...]}

        :return: string
        """

        addresses = [flight.device.address for flight in self.flights]
        warn = dict()
        uniq_addresses = list(set(addresses))
        if len(uniq_addresses) != 0:
            idx = 0
            for elt in self.red.hmget(name='STATE', keys=uniq_addresses):
                if elt is None:
                    warn[uniq_addresses[idx]] = False
                else:
                    status = Status(loads(elt).get('sts', None))
                    # if detected formally on ground on a known airfield, warn false
                    if status is status.ON_GROUND_AT:
                        warn[uniq_addresses[idx]] = False
                    # if detected in flight
                    elif status.in_flight:
                        warn[uniq_addresses[idx]] = True
                    else:
                        warn[uniq_addresses[idx]] = False
                idx += 1

        # privacy concerns, check against mapping address
        # find some privacy test cases with following sql requests
        # select * from Events where address IN (select address from Devices where tracked=0) and code is not NULL
        map_addresses = mapping_addresses(addresses=addresses, red=self.red)
        idx = 0
        for address in addresses:
            self.flights[idx].warn = warn[address]
            # case tracked = False => delete flight
            if map_addresses[idx] == '-1':
                self.flights[idx] = None
            # case not present in ddb or identified = False
            elif address != map_addresses[idx]:
                # create a generic device with obfuscated address
                self.flights[idx].device = Device(map_addresses[idx])
            idx += 1

        json = dict()
        json['airfield'] = self.airfield
        json['code'] = self.code
        json['date'] = self.date
        json['a_day'] = self.a_day
        devices_addr = OrderedDict()
        flights_id = list()
        jflights = list()

        for flight in self.flights:
            # ignore None flights (marked as deleted by mapping)
            if flight is None:
                continue
            flights_id.append(id(flight))
            jflight = dict()
            jflight['device'] = flight.device.address
            if flight.device.address not in devices_addr:
                devices_addr[flight.device.address] = flight.device
            if flight.start_c != self.code and flight.start_c is not None:
                jflight['start_code'] = flight.start_c
            jflight['start'] = flight.start_hm
            jflight['start_q'] = flight.start_q
            if flight.start_delta != 0:
                jflight['start_delta'] = flight.start_delta
            if flight.stop_c != self.code and flight.stop_c is not None:
                jflight['stop_code'] = flight.stop_c
            jflight['stop'] = flight.stop_hm
            jflight['stop_q'] = flight.stop_q
            if flight.stop_delta != 0:
                jflight['stop_delta'] = flight.stop_delta
            jflight['tow'] = id(flight.tow) if flight.tow is not None else None
            jflight['towing'] = flight.is_towing
            jflight['duration'] = flight.duration
            if flight.duration_expected is not None:
                jflight['duration_expected'] = flight.duration_expected
            jflight['max_alt'] = flight.max_alt
            jflight['max_height'] = abs(flight.max_alt - self.airfield.get("elevation", 0)) if \
                flight.max_alt is not None else None
            jflight['warn'] = flight.warn
            jflights.append(jflight)
        json['flights'] = jflights
        json["devices"] = [devices_addr[key].__dict__ for key in devices_addr]

        # replace object ref by index position in respective lists
        devices_addr_keys = list(devices_addr.keys())
        for jflight in json['flights']:
            jflight['device'] = devices_addr_keys.index(jflight['device'])
            if jflight['tow'] is not None:
                jflight['tow'] = flights_id.index(jflight['tow'])
        return json
