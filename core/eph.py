from datetime import datetime, timedelta
import ephem


class Eph(object):
    def __init__(self, lnglats, elev, date, tz):
        """

        :param lnglats: redis GEO:L key
        :param elev: int
        :param date: str date format: yyyy-MM-dd
        :param tz: Python pytz time zone
        """
        self._tz = tz
        info = dict()
        str_date = '{} 15:00:00'.format(date)
        eph_date = ephem.date(str_date)
        info["tz_name"] = str(tz)
        info["tz_offset"] = datetime.strftime(ephem.to_timezone(eph_date, tzinfo=tz), "%Z%z")

        obs = ephem.Observer()
        obs.date = eph_date
        obs.long = str(lnglats[0][0])
        obs.lat = str(lnglats[0][1])
        obs.elev = elev
        # avoid diffraction calculation with pressure = 0 & take std horizon value
        obs.pressure = 0
        obs.horizon = '-0:34'
        # noinspection PyUnresolvedReferences
        sunrise_utc = obs.previous_rising(ephem.Sun())
        # noinspection PyUnresolvedReferences
        noon_utc = obs.next_transit(ephem.Sun())
        # noinspection PyUnresolvedReferences
        sunset_utc = obs.next_setting(ephem.Sun())
        # civil twilight => horizon = -6, nautical twilight = -12 => , astronautical twilight = -18
        obs.horizon = '-6'
        # noinspection PyUnresolvedReferences
        dawn_utc = obs.previous_rising(ephem.Sun(), use_center=True)
        # noinspection PyUnresolvedReferences
        twilight_utc = obs.next_setting(ephem.Sun(), use_center=True)

        info["dawn"], info["sunrise"], info["noon"], info["sunset"], info['twilight'] = \
            map(self._to_tz, (dawn_utc, sunrise_utc, noon_utc, sunset_utc, twilight_utc))
        self.info = info

    def _to_tz(self, ephdt):
        # first convert ephem date to python datetime object
        dt = ephem.to_timezone(ephdt, self._tz)
        # round the result to the nearest minute and format
        return (dt + timedelta(seconds=30)).strftime('%Hh%M')
