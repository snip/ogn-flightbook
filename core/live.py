import json
from datetime import datetime

from utils.device import get_rdevice, mapping_address, mapping_addresses


ALIVE_TIME = 120        # constant (in sec) for beacon alive flag
FLEET_TIME = 46800      # constant (in sec) for beacon alive in fleet map


def convert_dms(value, type_):
    """
    short decimal to DMS position converter
    value: float
    type_: str ('lat' or 'lng')
    """
    if not isinstance(value, float):
        return
    degrees = int(value)
    submin = abs((value - degrees) * 60)
    minutes = int(submin)
    subseconds = abs((submin - minutes) * 60)
    direction = ""
    if type_ == "lng":
        direction = "W" if degrees < 0 else "E"
    elif type_ == "lat":
        direction = "S" if degrees < 0 else "N"

    return '{}° {}\' {:.0f}\'\' {}'.format(degrees, minutes, subseconds, direction)


def address(red, address_, privacy=True):
    """

    :param red: a redis connection object
    :param address_: str beacon address
    :param privacy: boolean, enable privacy
    :return: dict()
    """
    if address_ is None:
        return
    address_ = address_.upper()

    # privacy concerns
    map_address = mapping_address(address=address_, red=red) if privacy else address_

    key = 'BA:{}'.format(map_address)
    fix = red.lindex(key, -1)
    if fix is None:
        return
    fix = json.loads(fix)
    res = dict()
    res['lat'] = fix.get('lat', None)
    res['lng'] = fix.get('lng', None)
    res['lat_dms'], res['lng_dms'] = convert_dms(fix['lat'], 'lat'),  convert_dms(fix['lng'], 'lng')
    res['utc'] = datetime.utcfromtimestamp(fix['tsp']).strftime('%Y-%m-%d @ %H:%M:%S')
    res['alive'] = True if fix['tsp'] + ALIVE_TIME > datetime.now().timestamp() else False
    res['track'] = fix.get('track', None)
    res['alt'] = fix.get('alt', None)
    res['gsp'] = fix.get('gsp', None)
    res['clr'] = fix.get('clr', None)
    res['addr'] = address_
    device = get_rdevice(address=address_, red=red)
    res['acft'] = device.aircraft
    res['reg'] = device.registration
    res['cn'] = device.competition
    res['type'] = device.aircraft_type

    # TODO part
    res['receiv'] = "#TODO"
    return res


def maddress(red, addresses, privacy=True):
    """

    :param red: a redis connection object
    :param addresses: iterable str beacon address
    :param privacy: boolean, enable privacy
    :return: a list of dict()
    """
    res = list()
    if addresses is None:
        return None
    addresses = [elt.upper() for elt in addresses]

    # privacy concerns
    map_addresses = mapping_addresses(addresses=addresses, red=red) if privacy else addresses

    for addr in map_addresses:
        res.append(address(red=red, address_=addr, privacy=privacy))
    return [elt for elt in res if elt is not None]


def positions(red, addresses, strict=False):
    """
    return positions end ground speed for given addresses list
    :param red:
    :param addresses:
    :param strict:
    :return: list of dicts()
    """
    ret = list()
    if addresses is None:
        return ret
    addresses = [elt.upper() for elt in addresses]
    for address_ in addresses:
        key = 'BA:{}'.format(address_)
        fix = red.lindex(key, -1)
        if fix is None:
            continue
        fix = json.loads(fix)
        d = dict()
        d['lat'] = fix.get('lat', None)
        d['lng'] = fix.get('lng', None)
        d['gsp'] = fix.get('gsp', None)
        if d['gsp'] != 0:
            continue
        if strict:
            geor = red.georadius(name="GEO:L", longitude=d['lng'], latitude=d['lat'],
                                 radius=3, unit="km", withdist=True, count=1, sort='ASC')
            if len(geor) == 0:
                ret.append(d)
        else:
            ret.append(d)

    return ret


def fleet(red, code):
    """

    :param red: a redis connection
    :param code: str, OACI airfield code
    :return: a list of dict()
    """
    if code is None:
        return None
    code = code.upper()
    h = red.hgetall('FLEET:{}'.format(code))
    if h is None:
        return None
    addrs = list()
    del_addrs = list()
    now_ = datetime.now().timestamp()
    for addr in h:
        if int(h[addr]) + FLEET_TIME > now_:
            addrs.append(addr)
        else:
            del_addrs.append(addr)
    # delete redis hash item for next call & for saving memory space
    if len(del_addrs) >= 1:
        red.hdel('FLEET:{}'.format(code), *del_addrs)

    return maddress(red=red, addresses=addrs)


def path(red, address_, privacy=True):
    """

    :param red:
    :param address_:
    :param privacy: boolean, enable privacy
    :return:
    """

    if address_ is None:
        return
    address_ = address_.upper()

    # privacy concerns
    map_address = mapping_address(address=address_, red=red) if privacy else address_

    stream = 'SA:{}'.format(map_address)
    xr = red.xrange(name=stream)
    # xrange response like:
    # [('1601308752376-0', {'poly': '[[47.52022, 12.44962], [47.52017, 12.44965]]'}),
    # ('1601308809675-0', {'poly': '[[47.52022, 12.44962], [47.52017, 12.44965]]'})]
    polyline = list()
    if xr is None or len(xr) == 0:
        return

    for _, fields in xr:
        fragment = fields.get('poly', None)
        if fragment is not None:
            polyline.extend(json.loads(fragment))

    return {'poly': polyline}
