import json
from datetime import datetime
from collections import deque
import logging
from enum import Enum

L_SPEED = 50           # constant low speed detect
L_ALT = 80             # constant low alt detect
L_ALT_APPROX = 150     # constant low alt detect (approx detect case)
INTERVAL = 600         # constant max time analyze interval in s
RADIUS = 3             # constant RADIUS for airfield detection in km
RADIUS_APPROX = 6      # constant RADIUS for airfield detection in km (approx detect case)
codes = None


class Status(Enum):
    UNKNOWN = None
    ON_GROUND_AT = 0    # formally detected on ground on known airfeld
    IN_FLIGHT_FROM = 1  # formally detect in flight after takeoff on a knonwn airfield
    ON_GROUND = 2       # detected on ground on unknown airfield
    IN_FLIGHT = 3       # detected in flight after takeoff on unknonwn airfield

    @property
    def on_ground(self):
        return self.value == 0 or self.value == 2

    @property
    def in_flight(self):
        return self.value == 1 or self.value == 3


def log_evt(evt, buf):
    logging.info("--------------------")
    logging.info(evt)
    for _ in buf:
        logging.debug("{}".format(_))


def feed_queue(red, delete=True):
    """
    populate the redis QBA queue
    :param red: a redis connection
    :param delete: boolean
    :return:
    """
    # feed the QBA queue only when empty
    if red.exists('QBA') == 0:
        set_ = red.smembers(name='SQBA')
        if set_:
            if delete:
                # TODO add a watch / multi lock mechanism
                red.delete('SQBA')
            red.rpush('QBA', *set_)


def consum_queue(red, db, delete=True):
    """
    Consum the QBA & APRS queue => create & insert event in sql ddb.
    An event is a take_off or landing. Event is insert in sql database
    :param red: a redis connection
    :param db: database object
    :param delete: delete / not delete aprs qeue (use in debug mode)
    :return:
    """
    # codes is a dictionary codes['airfield_icao_code'] = airfield_elevation, time_zone index
    # push codes in memory and not solicit sql db each time function is call
    global codes
    if codes is None:
        codes = db.get_codes()
    # left pop and block until QBA not empty
    _, key = red.blpop('QBA')
    records = [json.loads(elt) for elt in red.lrange(name=key, start=0, end=-1)]
    # we need at least 4 elts for analyse
    if len(records) < 4:
        return

    # trim the queue as soon as possible, to avoid aprs messages lost
    if delete:
        # always keep the three last beacons for next run
        red.ltrim(key, -3, -1)

    address = str(key)[3:]
    stream = 'SA:{}'.format(address)
    polyline = list()

    # for polyline, get the latest tsp stored in redis stream
    last_tsp = 0
    xrr = red.xrevrange(name=stream, count=1)
    if xrr is not None and len(xrr) == 1:
        _, fields = xrr[0]
        tsp_ = fields.get('tsp', None)
        last_tsp = int(tsp_) if tsp_ is not None else 0

    # get the latest code, timestamp, max_level & status stored in redis 'STATE' key
    jstate = red.hget(name='STATE', key=address)
    state = json.loads(jstate) if jstate is not None else {'c': None, 't': 0, 'ml': 0, 'sts': None}
    status = Status(state.get("sts", None))

    buf = deque((), 4)

    for index_ in range(len(records)):
        cur_fix = records[index_]
        # evaluate the low_speed flag
        cur_fix['low_speed'] = cur_fix['gsp'] < L_SPEED

        # update the max elevation
        if cur_fix['alt'] > state['ml']:
            state['ml'] = cur_fix['alt']

        buf.append(cur_fix)

        # we need at least 4 points for event analyze
        if len(buf) < 4:
            continue

        # append fix on polyline only for given time delta (after reaching 4 fix)
        if buf[-1]['tsp'] - last_tsp > 20:
            # append fix on polyline only for new positions
            if buf[-1]['lat'] != buf[-2]['lat'] and buf[-1]['lng'] != buf[-2]['lng']:
                polyline.append((round(buf[-1]['lat'], 5), round(buf[-1]['lng'], 5)))
                last_tsp = buf[-1]['tsp']

        # go deeper only if two consecutive last point have the same low_speed flag
        if buf[-2]['low_speed'] != cur_fix['low_speed']:
            continue

        # Do not analyse for a max time Interval
        if cur_fix['tsp'] - buf[0]['tsp'] > INTERVAL:
            continue

        if not buf[0]['low_speed'] and not buf[1]['low_speed'] and buf[2]['low_speed']:
            # windy day, avoid double landing detection
            if status is Status.ON_GROUND_AT and abs(buf[2]['tsp'] - state['t']) < 20:
                continue
            geor = red.georadius(name="GEO:L", longitude=buf[2]['lng'], latitude=buf[2]['lat'],
                                 radius=RADIUS, unit="km", withdist=True, count=1, sort='ASC')
            qfu = round((buf[0]['track'] + buf[1]['track'])/20)
            l_code = None if len(geor) == 0 else geor[0][0]
            if l_code is None:
                db.insert_event(timestamp=buf[2]['tsp'], address=address, event=0, code=l_code,
                                tzidx=None, max_alt=state['ml'], qfu=qfu, approx=0)
                evt = ("Landing {}, {} @ {}".format(address, datetime.fromtimestamp(buf[2]['tsp']), l_code))
                state['t'] = buf[2]['tsp']
                status = Status.ON_GROUND
                log_evt(evt, buf)
            else:
                if abs(buf[2]['alt'] - codes[l_code][0]) < L_ALT:
                    db.insert_event(timestamp=buf[2]['tsp'], address=address, event=0, code=l_code,
                                    tzidx=codes[l_code][1], max_alt=state['ml'], qfu=qfu, approx=0)
                    red.hset('FLEET:{}'.format(l_code), address, buf[2]['tsp'])
                    red.xadd(name='SEVT', fields={l_code: 0}, approximate=True, maxlen=8)
                    state['ml'] = 0
                    state['c'] = l_code
                    state['t'] = buf[2]['tsp']
                    status = Status.ON_GROUND_AT
                    evt = ("Landing {}, {} @ {}".format(address, datetime.fromtimestamp(buf[2]['tsp']), l_code))
                    log_evt(evt, buf)

        elif buf[0]['low_speed'] and buf[1]['low_speed'] and not buf[2]['low_speed']:
            # windy day, avoid double takeoff detection
            if status is Status.IN_FLIGHT_FROM and abs(buf[2]['tsp'] - state['t']) < 20:
                continue

            geor = red.georadius(name="GEO:L", longitude=buf[2]['lng'], latitude=buf[2]['lat'],
                                 radius=RADIUS, unit="km", withdist=True, count=1, sort='ASC')
            qfu = round((buf[1]['track'] + buf[2]['track']) / 20)
            t_code = None if len(geor) == 0 else geor[0][0]
            if t_code is None:
                db.insert_event(timestamp=buf[2]['tsp'], address=address, event=1, code=t_code,
                                tzidx=None, max_alt=None, qfu=qfu, approx=0)
                evt = ("TakeOff {}, {} @ {}".format(address, datetime.fromtimestamp(buf[2]['tsp']), t_code))
                state['t'] = buf[2]['tsp']
                status = Status.IN_FLIGHT
                log_evt(evt, buf)
            else:
                if abs(buf[2]['alt'] - codes[t_code][0]) < L_ALT:
                    db.insert_event(timestamp=buf[2]['tsp'], address=address, event=1, code=t_code,
                                    tzidx=codes[t_code][1], max_alt=None, qfu=qfu, approx=0)
                    red.hset('FLEET:{}'.format(t_code), address, buf[2]['tsp'])
                    red.xadd(name='SEVT', fields={t_code: 1}, approximate=True, maxlen=8)
                    state['ml'] = 0
                    state['c'] = t_code
                    state['t'] = buf[2]['tsp']
                    status = Status.IN_FLIGHT_FROM
                    evt = ("TakeOff {}, {} @ {}".format(address, datetime.fromtimestamp(buf[2]['tsp']), t_code))
                    log_evt(evt, buf)

        # last tree fix @ same low_speed flag
        elif cur_fix['low_speed'] == buf[1]['low_speed']:
            if cur_fix['low_speed'] and status.on_ground:
                state['t'] = buf[3]['tsp']
                continue
            elif cur_fix['low_speed'] and status.in_flight:
                geor = red.georadius(name="GEO:L", longitude=buf[1]['lng'], latitude=buf[1]['lat'],
                                     radius=RADIUS_APPROX, unit="km", withdist=True, count=1, sort='ASC')
                qfu = round((buf[1]['track'] + buf[2]['track']) / 20)
                l_code = None if len(geor) == 0 else geor[0][0]
                if l_code is None:
                    evt = ("On_GROUND 3pts {}, {} @ {}".format(address, datetime.fromtimestamp(buf[1]['tsp']), l_code))
                    state['t'] = buf[3]['tsp']
                    status = Status.ON_GROUND
                    log_evt(evt, buf)
                else:
                    if abs(buf[1]['alt'] - codes[l_code][0]) < L_ALT_APPROX:
                        approx = buf[1]['tsp'] - state['t']
                        db.insert_event(timestamp=buf[1]['tsp'], address=address, event=0, code=l_code,
                                        tzidx=codes[l_code][1], max_alt=state['ml'], qfu=qfu, approx=approx)
                        red.hset('FLEET:{}'.format(l_code), address, buf[1]['tsp'])
                        red.xadd(name='SEVT', fields={l_code: 0}, approximate=True, maxlen=8)
                        state['ml'] = 0
                        state['c'] = l_code
                        state['t'] = buf[3]['tsp']
                        status = Status.ON_GROUND_AT
                        evt = ("Landing3pts {}, {} @ {}".format(address, datetime.fromtimestamp(buf[2]['tsp']), l_code))
                        log_evt(evt, buf)

            elif not cur_fix['low_speed'] and status.in_flight:
                state['t'] = buf[3]['tsp']
                continue

            elif not cur_fix['low_speed'] and status.on_ground:
                geor = red.georadius(name="GEO:L", longitude=buf[1]['lng'], latitude=buf[1]['lat'],
                                     radius=RADIUS_APPROX, unit="km", withdist=True, count=1, sort='ASC')
                qfu = round((buf[1]['track'] + buf[2]['track']) / 20)
                t_code = None if len(geor) == 0 else geor[0][0]
                if t_code is None:
                    evt = ("IN_FLIGHT3pts {}, {} @ {}".format(address, datetime.fromtimestamp(buf[1]['tsp']), t_code))
                    state['t'] = buf[3]['tsp']
                    status = Status.IN_FLIGHT
                    log_evt(evt, buf)
                else:
                    # for approx takeoff, altitude is not checked but instead we check last known ground position
                    if status is Status.ON_GROUND_AT and t_code == state['c']:
                        approx = buf[1]['tsp'] - state['t']
                        db.insert_event(timestamp=buf[1]['tsp'], address=address, event=1, code=t_code,
                                        tzidx=codes[t_code][1], max_alt=None, qfu=qfu, approx=approx)
                        red.hset('FLEET:{}'.format(t_code), address, buf[1]['tsp'])
                        red.xadd(name='SEVT', fields={t_code: 1}, approximate=True, maxlen=8)
                        state['ml'] = 0
                        state['c'] = t_code
                        state['t'] = buf[3]['tsp']
                        status = Status.IN_FLIGHT_FROM
                        evt = ("TakeOff3pts {}, {} @ {}".format(address, datetime.fromtimestamp(buf[1]['tsp']), t_code))
                        log_evt(evt, buf)

        else:
            continue

    # keep last state values in redis for next run (store state)
    state['sts'] = status.value
    red.hset(name='STATE', key=address, value=json.dumps(state))
    # create/append associated stream
    fields = dict()
    if len(polyline) > 0:
        fields['poly'] = json.dumps(polyline)
        fields['tsp'] = last_tsp
        red.xadd(name=stream, fields=fields, maxlen=300, approximate=True)
