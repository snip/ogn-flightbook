package main

import (
	"log"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

type Event struct {
	Code string `json:"code"`
	Evt  int    `json:"evt"`
}

func init_redis() (rdb *redis.Client) {
	rdb = redis.NewClient(&redis.Options{
		Addr:         Redis_addr,
		DialTimeout:  10 * time.Second,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		PoolSize:     3,
		PoolTimeout:  30 * time.Second,
	})
	_, err := rdb.Ping().Result()
	if err != nil {
		log.Fatal(err)
	}
	return rdb
}

func xread(rdb *redis.Client, pool *Pool) {
	var msg Message
	var evt Event
	for {
		res, err := rdb.XRead(&redis.XReadArgs{
			Streams: []string{Redis_stream, "$"},
			Count:   0,
			Block:   0,
		}).Result()
		if err != nil {
			log.Fatal(err)
		}
		xstream := res[0]
		xmessage := xstream.Messages[0]
		values := xmessage.Values

		for key, value := range values {
			value_int, _ := strconv.Atoi(value.(string))
			evt.Code = key
			evt.Evt = value_int
		}
		msg.Type = 1
		msg.Codevt = []Event{evt}
		pool.Broadcast <- msg
	}
}

func xrev(rdb *redis.Client) (evts []Event) {

	res, err := rdb.XRevRangeN(Redis_stream, "+", "-", 4).Result()
	if err != nil {
		log.Fatal(err)
	}
	lenres := len(res)

	for idx, _ := range res {
		xmessage := res[lenres-1-idx]
		values := xmessage.Values

		var code string
		var evt int
		for key, value := range values {
			value_int, _ := strconv.Atoi(value.(string))
			code = key
			evt = value_int
		}
		evts = append(evts, Event{code, evt})
	}
	return evts
}
