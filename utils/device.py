import csv
from io import StringIO
import hashlib
import logging
from datetime import date
import json

import requests

import setting

"""
    AircraftType:
    UNKNOWN = 0
    GLIDER_OR_MOTOR_GLIDER = 1
    TOW_TUG_PLANE = 2
    HELICOPTER_ROTORCRAFT = 3
    PARACHUTE = 4
    DROP_PLANE = 5
    HANG_GLIDER = 6
    PARA_GLIDER = 7
    POWERED_AIRCRAFT = 8
    JET_AIRCRAFT = 9
    FLYING_SAUCER = 10
    BALLOON = 11
    AIRSHIP = 12
    UNMANNED_AERIAL_VEHICLE = 13
    STATIC_OBJECT = 15
"""

OGN_URL = "http://ddb.glidernet.org/download/?t=1"
FLARMNET_URL = "https://www.flarmnet.org/static/files/wfn/data.fln"


class Device(object):
    def __init__(self, address):
        self.address = address
        self.address_type = None
        self.aircraft = None
        self.registration = None
        self.competition = None
        self.tracked = False
        self.identified = False
        self.aircraft_type = 0
        self.address_origin = None


def get_ogn(csv_file=None, red=None):

    today = str(date.today())
    if csv_file is None:
        try:
            r = requests.get(OGN_URL)
            if r.status_code != 200:
                raise Exception('bad status code response from ogn database url')
        except BaseException as e:
            logging.warning('fetch ogn failed: {}'.format(e))
            return None
        if red is not None:
            # sign the file. Date of day is append to file content for automatic update sign
            sign = md5sum(content=r.content + today.encode())
            # try to get old sign from redis db
            old_sign = red.get('SIGN:OGN')
            print('get old OGN db sign: {}'.format(old_sign))
            if old_sign is not None and old_sign == sign:
                print('retrieving ogn data base sign unchanged. Nothing to do')
                return None
            elif old_sign is None or old_sign != sign:
                print('storing new ogn database sign: {}'.format(sign))
                red.set(name='SIGN:OGN', value=sign)

        rows = "\n".join(i for i in r.text.splitlines() if i[0] != "#")

    else:
        try:
            r = open(csv_file, "r")
            rows = "".join(i for i in r.readlines() if i[0] != "#")
            r.close()
        except BaseException as e:
            logging.warning('reading {} file failed: {}'.format(csv_file, e))
            return None

    data = csv.reader(StringIO(rows), quotechar="'", quoting=csv.QUOTE_ALL)

    devices = list()
    for row in data:
        device = Device(address=row[1])
        device.address_type = row[0]
        device.aircraft = row[2]
        device.registration = row[3].upper().replace(" ", "") if row[3] else ''
        if row[4]:
            device.competition = row[4].upper().replace(" ", "")
        # TODO maybe add a false CN based on registration if need (livemap correlation)
        # else:
        #    device.competition = device.registration[-2:] if device.registration else ''
        device.tracked = row[5] == "Y"
        device.identified = row[6] == "Y"
        device.aircraft_type = int(row[7])
        device.address_origin = "OGN"
        devices.append(device)

    return devices


def get_flarmnet(fln_file=None):
    """
    Historical Function
    Not used anymore as flarmnet do not authorized database usage.
    :param fln_file:
    :return:
    """
    if fln_file is None:
        r = requests.get(FLARMNET_URL)
        rows = [bytes.fromhex(line).decode("latin1") for line in r.text.split("\n") if len(line) == 172]
    else:
        with open(fln_file, "r") as file:
            rows = [bytes.fromhex(line.strip()).decode("latin1") for line in file.readlines() if len(line) == 172]

    devices = list()
    for row in rows:
        device = Device(address=row[0:6].strip())
        device.aircraft = row[48:69].strip()
        device.registration = row[69:76].strip()
        device.competition = row[76:79].strip()
        device.address_origin = "FLNET"
        devices.append(device)

    return devices


def download_devices(db, red):
    """

    :param db: a database object
    :param red: redis client
    :return:int -1 if nothing is done 0 if insert / update is done
    """
    print("Fetch Ogn database @ {}".format(OGN_URL))
    devices = get_ogn(csv_file=None, red=red)
    if devices is None:
        print('devices: sql db & redis db unchanged')
        return -1

    print("{} records".format(len(devices)))
    logging.warning("insert/update devices in sql database")
    db.replace_devices(devices=devices)
    logging.warning("create/update devices in redis")
    set_rdevices(devices=devices, red=red)
    logging.warning("create/update redis devices mapping")
    mapping_devices(devices=devices, red=red)
    return 0


def set_rdevices(devices, red):
    """
    store in Redis a hash (name: DEV) as:
    hash[address] = json_string(Device obj)
    old hash entries are deleted after update
    :param devices: iterable of devices obj
    :param red: redis connection
    :return:
    """
    if devices is None:
        return
    store = dict()
    news = list()
    for device in devices:
        address = device.address.upper()
        news.append(address)
        store[address] = json.dumps(device.__dict__)
    if len(store) == 0:
        return
    # get the old entries
    olds = red.hkeys(name='DEV')
    # get elements to remove
    to_del = list(set(olds) - set(news))
    red.hmset(name='DEV', mapping=store)
    if len(to_del) != 0:
        red.hdel('DEV', *to_del)


def get_rdevice(address, red):
    """
    :param address: string
    :param red: redis client
    :return: Device Object
    """
    if address is None:
        return
    store = red.hget(name='DEV', key=address)
    device = Device(address)
    if store is not None:
        store_dict = json.loads(store)
        for key, value in store_dict.items():
            setattr(device, key, value)
    return device


def mapping_address(address, red):
    """

    :param address:
    :param red:
    :return:
    """
    if address is None:
        return

    value = red.hget(name='MAP', key=address)
    if value is not None:
        ret = value
    else:
        h = md5(address=address, prefix='~')
        to_store = dict()
        to_store[address] = h
        to_store[h] = address
        red.hmset(name='MAP', mapping=to_store)
        ret = h

    logging.debug('mappping: {} => {}'.format(address, ret))
    return ret


def mapping_addresses(addresses, red):
    """

    :param addresses: list() , a list of addresses
    :param red: redis client
    :return: list(), a list of mapping address
    """

    to_store = dict()
    ret = list()
    logging.debug('addresses before mapping: {}'.format(addresses))
    if len(addresses) == 0:
        return ret
    values = red.hmget('MAP', addresses)
    idx = 0
    for val in values:
        if val is not None:
            ret.append(val)
        else:
            h = md5(address=addresses[idx], prefix='~')
            ret.append(h)
            to_store[addresses[idx]] = h
            to_store[h] = addresses[idx]
        idx += 1
    if len(to_store) != 0:
        red.hmset(name='MAP', mapping=to_store)
    logging.debug('addresses after mapping: {}'.format(ret))
    return ret


def mapping_devices(devices, red):
    """

    :param devices: list()
    :param red: redis client
    :return: None
    """
    redhash = dict()
    for device in devices:
        if not device.tracked:
            redhash[device.address] = -1
            continue
        # if identified is True, create a single entry in redis: address=>address
        if device.identified:
            redhash[device.address] = device.address
        else:
            # if identified is false, create double entries in redis: address=>hash(address) & hash(address)=>address
            h = md5(address=device.address, prefix='_')
            redhash[device.address] = h
            redhash[h] = device.address

    if len(redhash) != 0:
        # delete the old redis mapping
        red.delete('MAP')
        # set the new redis mapping
        red.hset(name='MAP', mapping=redhash)


def md5(address, prefix):
    today = date.today()
    salt = '{}{}'.format(setting.PRIVACY_SALT, today)
    m = hashlib.md5()
    m.update(('{}{}'.format(address, salt)).encode())
    ret = m.digest().hex().upper()
    return '{}{}'.format(prefix, ret[:10])


def md5sum(content):
    m = hashlib.md5()
    m.update(content)
    return m.hexdigest()
