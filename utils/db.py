import sqlite3
from datetime import datetime, date, timedelta
from os import path as os_path

from pytz import timezone as pytz_timezone, all_timezones


class Db(object):
    def __init__(self, path=None):
        if path is None:
            self.path = os_path.join(os_path.dirname(os_path.abspath(__file__)), '../db.sqlite')
        else:
            self.path = path
        self.conn = sqlite3.connect(self.path, timeout=7)
        self.__init_events()
        self.__init_devices()
        self.__init_airfields()

    def __init_events(self):
        cursor = self.conn.cursor()
        cursor.execute("""
                CREATE TABLE IF NOT EXISTS Events(
                    `address` TEXT,
                    `code` TEXT,
                    `event` INT, 
                    `timestamp` INT,
                    `date` TEXT,
                    `tr` REAL,
                    `max_alt` INT,
                    `qfu` INT,
                    `approx` INT);
                """)
        cursor.execute("""CREATE INDEX IF NOT EXISTS Idx_Events ON Events(date, code);""")
        self.conn.commit()

    def __init_devices(self):
        cursor = self.conn.cursor()
        cursor.execute("""
                        CREATE TABLE IF NOT EXISTS Devices(
                            `address` TEXT NOT NULL PRIMARY KEY,
                            `address_type` TEXT,
                            `aircraft` TEXT,
                            `registration` TEXT,
                            `competition` TEXT,
                            `tracked` INT,
                            `identified` INT,
                            `aircraft_type` INT,
                            `address_origin` TEXT);
                        """)
        self.conn.commit()

    def __init_airfields(self):
        cursor = self.conn.cursor()
        cursor.execute("""
                            CREATE TABLE IF NOT EXISTS Airfields(
                            `code` TEXT NOT NULL PRIMARY KEY,
                            `name` TEXT, `elevation` INT, `tz` TEXT, `tzidx` INT, `country` TEXT);
                        """)
        self.conn.commit()

    def replace_airfields(self, codes):
        cursor = self.conn.cursor()
        for code in codes:
            cursor.execute("REPLACE INTO Airfields(`code`,`name`,`elevation`, `tz`, `tzidx`, `country`)"
                           " VALUES (?,?,?,?,?,?)",
                           (code, codes[code][0], codes[code][1], codes[code][2], codes[code][3], codes[code][4])
                           )
        self.conn.commit()

    def delete_airfields(self):
        cursor = self.conn.cursor()
        cursor.execute("""DROP TABLE IF EXISTS Airfields;""")
        self.conn.commit()

    def get_codes(self):
        codes = dict()
        cursor = self.conn.cursor()
        cursor.execute("SELECT code, elevation, tzidx from Airfields;")
        rows = cursor.fetchall()
        for code, elevation, tzidx in rows:
            codes[code] = elevation, tzidx
        return codes

    def search_airfield(self, query, limit=None):
        if query is None:
            return None
        pattern = '%{}%'.format(query)
        cursor = self.conn.cursor()
        # first try with exact airfield ICAO code
        cursor.execute("select code, name, elevation, tz from Airfields where code = ? LIMIT 1",
                       (query.upper(), ))
        resp = cursor.fetchall()
        # then expend search with regex
        cursor.execute("select code, name, elevation, tz from Airfields where "
                       "name like ? or code like ? and code != ? LIMIT ?",
                       (pattern, pattern, query.upper(), limit))
        resp.extend(cursor.fetchall())
        return resp

    def match_airfield(self, code):
        if code is None:
            return None
        cursor = self.conn.cursor()
        cursor.execute("select `name`, `elevation`, `tz`, `country` from Airfields where code=?", (code, ))
        resp = cursor.fetchone()
        return resp

    def replace_devices(self, devices):
        cursor = self.conn.cursor()
        for device in devices:
            cursor.execute("""
                            REPLACE INTO Devices(
                                `address`,
                                `address_type`,
                                `aircraft`,
                                `registration`,
                                `competition`,
                                `tracked`,
                                `identified`,
                                `aircraft_type`,
                                `address_origin`) VALUES (?,?,?,?,?,?,?,?,?)""",
                           (device.address, device.address_type, device.aircraft, device.registration,
                            device.competition, device.tracked, device.identified, device.aircraft_type,
                            device.address_origin))
        self.conn.commit()

    def close(self):
        self.conn.close()

    def delete_events(self, all_=False, null_=True, before=None):
        cursor = self.conn.cursor()
        if all_:
            cursor.execute("""DROP TABLE IF EXISTS Events;""")
            self.conn.commit()
            return

        if before is not None and null_:
            cursor.execute("""DELETE FROM Events where date < (?) and code IS NULL""", (before,))
            self.conn.commit()
            return

        if before is not None and not null_:
            cursor.execute("""DELETE FROM Events where date < (?)""", (before,))
            self.conn.commit()
            return

    def delete_devices(self):
        cursor = self.conn.cursor()
        cursor.execute("""DROP TABLE IF EXISTS Devices;""")
        self.conn.commit()

    def delete(self):
        self.delete_events(all_=True)
        self.delete_devices()
        self.delete_airfields()

    def insert_event(self, timestamp, address, event, code, tzidx, max_alt, qfu, approx):
        tz = pytz_timezone(all_timezones[tzidx]) if tzidx is not None else None
        datetime_ = datetime.fromtimestamp(timestamp, tz=tz)
        year = datetime_.year
        month = '{:02}'.format(datetime_.month)
        day = '{:02}'.format(datetime_.day)
        date_ = '{}-{}-{}'.format(year, month, day)
        cursor = self.conn.cursor()
        # TODO remove tr
        cursor.execute("""
                        INSERT INTO Events (address,code,event,timestamp,date,tr, max_alt, qfu, approx) 
                        VALUES(?,?,?,?,?,?,?,?,?)
                        """,
                       (address,  code, event, timestamp, date_, None, max_alt, qfu, approx))
        self.conn.commit()

    def insert_event_lines(self, records):
        """
        Insert multiple lines in Events table (maintenance function purpose)
        :param records: iterable tuples
        :return:
        """
        cursor = self.conn.cursor()
        cursor.executemany("INSERT INTO Events VALUES(?,?,?,?,?,?,?,?,?);", records)
        print("insert {} records into Event table, db {}".format(cursor.rowcount, self.path))
        self.conn.commit()

    def get_events(self, date_, code):
        cursor = self.conn.cursor()
        cursor.execute("""
                select address, code, event, timestamp, max_alt, qfu, approx from Events where
                address IN (select address from Events where date=? and code=?) and date=? and code IS NOT NULL
                order by timestamp;
                """, (date_, code, date_))
        events = cursor.fetchall()
        self.conn.commit()
        return events

    def get_events_all(self):
        """
        Query all rows in Events table (maintenance function purpose)
        :return: list of tuples
        """
        cursor = self.conn.cursor()
        cursor.execute("select * From Events")
        res = cursor.fetchall()
        print("read {} records from Events table in db {}".format(len(res), self.path))
        return res

    def vacuum(self):
        cursor = self.conn.cursor()
        cursor.execute("VACUUM")
        self.conn.commit()

    def search_null(self, minus_day=None, strict=False):
        cursor = self.conn.cursor()
        if minus_day is not None:
            date_ = date.today() - timedelta(days=minus_day)
            if not strict:
                cursor.execute("""
                SELECT DISTINCT address from Events where code IS NULL and date > (?)
                """, (str(date_),))
            else:
                cursor.execute("""
                SELECT DISTINCT address from Events where code IS NULL and date > (?)
                and address NOT IN (select distinct address from Events where code is not null and date > (?))
                """, (str(date_), str(date_)))
        else:
            if not strict:
                cursor.execute(""" SELECT DISTINCT address from Events where code IS NULL""")
            else:
                cursor.execute(""" SELECT DISTINCT address from Events where code IS NULL 
                and address NOT IN (select distinct address from Events where code is not null)""")

        addresses = [elt[0] for elt in cursor.fetchall()]
        return addresses
