#!/usr/bin/env python
from datetime import datetime
import os
import sys

from pytz import timezone as pytz_timezone, utc as pytz_utc
import click
import redis
from tabulate import tabulate

# python workaround importing from parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from core.logbook import Logbook
from utils.db import Db
import setting


class CLogbook(Logbook):
    """
    a SubClass of LogBook with print method for cli usage
    """
    def __init__(self, date, code, red, db, tz):
        Logbook.__init__(self, date, code, red, db, tz)

    @staticmethod
    def none2str(elt):
        if elt is None:
            return 'UNKNOWN'
        else:
            return elt

    def _base_row(self, flight, show_add=False, verbose=False):
        if flight.start_hm is None:
            start_hm = None
        else:
            start_hm = str()
            if flight.start_a and verbose:
                start_hm = ('~{}\n'.format(flight.start_a))
            if flight.start_c is not None:
                start_hm += flight.start_hm if flight.start_c == self.code else \
                    '\n@'.join((flight.start_hm, flight.start_c))
            if flight.start_delta:
                if flight.start_delta < 60:
                    start_hm = '{} +-({})'.format(start_hm, flight.start_delta)
                else:
                    start_hm = '{} +-(??)'.format(start_hm)

        if flight.stop_hm is None:
            stop_hm = None
        else:
            stop_hm = str()
            if flight.stop_a and verbose:
                stop_hm = ('~{}\n'.format(flight.stop_a))
            if flight.stop_c is not None:
                stop_hm += flight.stop_hm if flight.stop_c == self.code else \
                    '\n@'.join((flight.stop_hm, flight.stop_c))
            if flight.stop_delta:
                stop_hm = '{} +-({})'.format(stop_hm, flight.stop_delta)

        duration_hm = flight.duration_hm
        if flight.start_delta or flight.stop_delta and flight.duration is not None:
            if flight.start_delta < 60:
                duration_hm = '{} +-({})'.format(duration_hm, (flight.start_delta+flight.stop_delta))
            else:
                duration_hm = '{} +-({})'.format(duration_hm, '??')

        row_items = [flight.device.registration, flight.device.aircraft, start_hm, stop_hm, duration_hm]

        if show_add:
            row_items.insert(0, flight.device.address)
        row_items = [self.none2str(elt) for elt in row_items]
        return row_items

    def _ext_raw(self, flight, show_add=False, verbose=False):

        if flight.stop_hm is None:
            stop_hm = None
        else:
            stop_hm = str()
            if flight.stop_a and verbose:
                stop_hm = ('~{}\n'.format(flight.stop_a))
            if flight.stop_c is not None:
                stop_hm += flight.stop_hm if flight.stop_c == self.code else \
                    '\n@'.join((flight.stop_hm, flight.stop_c))
            if flight.stop_delta:
                stop_hm = '{} +-({})'.format(stop_hm, flight.stop_delta)

        duration_hm = flight.duration_hm
        if flight.start_delta or flight.stop_delta and flight.duration is not None:
            if flight.start_delta < 60:
                duration_hm = '{} +-({})'.format(duration_hm, (flight.start_delta + flight.stop_delta))
            else:
                duration_hm = '{} +-({})'.format(duration_hm, '??')

        row_items = [flight.device.registration, flight.device.aircraft, stop_hm, duration_hm]
        if show_add:
            row_items.insert(0, flight.device.address)
        row_items = [self.none2str(elt) for elt in row_items]
        return row_items

    def print(self, show_add=False, no_tow=False, glider_only=False, verbose=False):
        print("LogBook {} {} @ {}:".format(self.a_day, self.date, self.code))
        rows = list()
        flights_ = [elt for elt in self.flights if elt.device.aircraft_type == 1] if glider_only else self.flights
        if no_tow:
            if show_add:
                headers = ('Addr', 'Registr', 'Aircraft', 'Start', 'Stop', 'Duration')
            else:
                headers = ('Registr', 'Aircraft', 'Start', 'Stop', 'Duration')
            for flight in flights_:
                row = self._base_row(flight, show_add=show_add, verbose=verbose)
                rows.append(row)
            print(tabulate(rows, headers=headers, tablefmt="grid"))

        else:
            if show_add:
                headers = ('Addr', 'Registr', 'Aircraft', 'Start', 'Stop', 'Duration',
                           'Addr', 'Registr', 'Aircraft', 'Stop', 'Duration', 'Aircraft')
            else:
                headers = ('Registr', 'Aircraft', 'Start', 'Stop', 'Duration',
                           'Registr', 'Aircraft', 'Stop', 'Duration', 'Aircraft')
            for flight in flights_:
                if flight.is_towing:
                    continue
                elif flight.tow is not None:
                    row = self._base_row(flight, show_add=show_add, verbose=verbose) + \
                          self._ext_raw(flight.tow, show_add=show_add, verbose=verbose)
                else:
                    row = self._base_row(flight, show_add=show_add, verbose=verbose)
                rows.append(row)
            print(tabulate(rows, headers=headers, tablefmt="grid"))


@click.command()
@click.argument('code')
@click.option('-d', '--date', help='use date in format like 2020_07_17 or 2020-07-17. '
                                   'Take date of day for corresponding airfield if missing')
@click.option('-a', '--address', is_flag=True, help='show beacon address')
@click.option('-nt', '--no-tow', is_flag=True, help='do not associate tow')
@click.option('-go', '--glider-only', is_flag=True, help='just show glider and associated tow')
@click.option('-v', '--verbose', is_flag=True, help='verbose mode, show addition informations')
def main(code, date, address, no_tow, glider_only, verbose):
    """
    Display LogBook for a given ICAO airfield code (upper or lower case).
    """
    red = redis.Redis(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                      decode_responses=True, encoding="utf-8")
    # noinspection PyBroadException
    try:
        red.ping()
    except BaseException:
        print("error while testing redis connection")
        print("check redis server is up and running")
        exit(1)

    code = code.upper()
    db = Db(path=setting.SQLITE_PATH)
    match_airfield = db.match_airfield(code)
    tz = pytz_timezone(match_airfield[2]) if match_airfield is not None else pytz_utc
    if date is None:
        date = datetime.now(tz).strftime('%Y-%m-%d')
    logbook = CLogbook(date=date, code=code, red=red, db=db, tz=tz)
    logbook.create()
    logbook.print(show_add=address, no_tow=no_tow, glider_only=glider_only, verbose=verbose)
    red.close()
    db.close()


if __name__ == "__main__":
    main()
