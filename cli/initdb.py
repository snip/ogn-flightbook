#!/usr/bin/env python
import logging
from datetime import date, timedelta
import os
import sys

import click
import redis

# python workaround importing from parent directory
sys.path.append(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from utils.feedcup import feed_dir
from utils.device import download_devices
from utils.db import Db
import setting


@click.command()
@click.option('-de', '--delete-events', type=int,
              help='delete events previously to a number of days, preserve non null code')
@click.option('-def', '--delete-events-force', type=int,
              help='delete events previously to a number of days')
@click.option('-ape', '--append-events', type=click.Path(exists=True),
              help='append events from another db specified by path name')
@click.option('-acs', '--adding-cups', help='adding all cups files', is_flag=True)
@click.option('-ac', '--adding-cup', help='adding specific cup file')
@click.option('-ad', '--adding-devices', help='adding devices', is_flag=True)
@click.option('-dd', '--delete-devices', help='delete devices', is_flag=True)
@click.option('-rc', '--red-clean', help='redis clean old streams', is_flag=True)
def main(delete_events, delete_events_force, append_events, adding_cups,
         adding_cup, adding_devices, delete_devices, red_clean):

    logging.basicConfig(level=logging.WARNING)
    db = Db(path=setting.SQLITE_PATH)
    red = redis.Redis(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                      decode_responses=True, encoding="utf-8")
    try:
        red.ping()
    except BaseException as e:
        print("Error while testing redis connection")
        print("Check redis server is up and running")
        logging.debug(str(e))
        exit(1)

    if delete_events:
        date_ = subdate(delete_events)
        print("delete null events for date before {}".format(date_))
        db.delete_events(before=date_)
        db.vacuum()

    if delete_events_force:
        date_ = subdate(delete_events_force)
        print("delete events for date before {}".format(date_))
        db.delete_events(before=date_, null_=False)
        db.vacuum()

    if append_events:
        db_new_path = click.format_filename(append_events)
        if db_new_path == db.path:
            print("sqlite db paths must be different: {}".format(db.path))
            return
        db_new = Db(path=db_new_path)
        print("actual events base: {}".format(db.path))
        print("new events base: {}".format(db_new.path))
        answer = input("Append new events to actual base[yes/N]? ").lower()
        if answer != "yes":
            print("abort")
            return
        records = db_new.get_events_all()
        db.insert_event_lines(records=records)

    if adding_cups:
        print("adding airfields location and altitude in Redis Database and Sql Database")
        feed_dir(db=db, red=red)
        # TODO implement signal thread (or in main loop event) in consumer
        print("relaunch consumer(s) process for loading new data")

    if adding_cup:
        print("adding airfields location and altitude in Redis Database and Sql Database")
        feed_dir(db=db, red=red, filename=adding_cup)

    if adding_devices:
        print("Adding devices")
        download_devices(db=db, red=red)

    if delete_devices:
        print("Delete devices")
        red.delete('SIGN:OGN')
        db.delete_devices()

    if red_clean:
        print("Redis Sreams cleaner")
        redclean(red=red)


def subdate(nb):
    """
    sub a number of days to actual date and return a date format 'YYYY-MM-dd'
    :param nb: int numbers of days to sub
    :return: str
    """
    ret = date.today() - timedelta(days=nb)
    return ret


def redclean(red):
    """
    Clean redis Stream older than 2 weeks
    :param red: redis client instance
    :return:
    """
    IDLE = 3600*24*15               # last 15 days
    streams = list()
    del_streams = list()
    total_mem = 0

    for key in red.keys(pattern="BA:*"):
        addr = key[3:]
        stream = 'SA:{}'.format(addr)
        streams.append(stream)

    for stream in streams:
        idle = red.object(infotype="idletime", key=stream)
        if idle is not None and idle > IDLE:
            mem_use = red.memory_usage(key=stream)
            if mem_use is not None:
                total_mem += mem_use
            del_streams.append(stream)

    if del_streams:
        print("total streams to remove: {}".format(len(del_streams)))
        print("corresponding memory bytes: {}".format(total_mem))
        answer = input("Remove redis streams[yes/N]? ").lower()
        if answer != "yes":
            print("Abort removing")
        else:
            red.delete(*del_streams)
    else:
        print("nothing to remove")


if __name__ == "__main__":
    main()
